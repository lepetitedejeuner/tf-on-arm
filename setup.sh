#!/bin/sh
# We can't get it from pip for ARM :-(
#sudo apt install -y python3 python3-pip
#pip3 install tensorflow

# Install dependencies
# Thanks to: https://github.com/lhelontra/tensorflow-on-arm
sudo apt-get install -y openjdk-8-jdk automake autoconf curl zip unzip \
                        libtool swig libpng-dev zlib1g-dev pkg-config git \
                        g++ wget xz-utils python3-numpy python3-dev \
                        python3-pip python3-mock
pip3 install -U --user keras_applications==1.0.5 --no-deps
pip3 install -U --user keras_preprocessing==1.0.3 --no-deps

# More instructions from the tensorflow-on-arm repo...
sudo dpkg --add-architecture armhf
echo "deb [arch=armhf] http://httpredir.debian.org/debian/ buster main contrib non-free" | sudo tee -a /etc/apt/sources.list
sudo apt-get install -y libpython3-all-dev

git clone https://github.com/lhelontra/tensorflow-on-arm
cd tensorflow-on-arm
cd build_tensorflow/
chmod +x build_tensorflow.sh
TF_PYTHON_VERSION=3.5 ./build_tensorflow.sh configs/odroidc2.conf
